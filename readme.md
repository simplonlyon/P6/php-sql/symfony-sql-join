# Symfony SQL join

## Quick start
1. `composer install`
1. `docker-compose up`
1. go to http://localhost:8080

## Exercices
### SQL
1. Créer une table `user` avec un `id`, `name`:varchar(45), `surname`:varchar(45), `email`:varchar(45), `birthdate`:datetime, `gender`:int
2. Créer une table product avec `id`, `name`:varchar(45), `description`:text, `price`:decimal(15,2)
3. Créer une table `shopping_cart` avec `user_id`, `product_id`, `count`:int. `user_id` et `product_id` sont respectivement des clés étrangères de `user.id` et de `product.id`. Le couple `user_id` et `product_id` est également clé primaire de la table `shopping_cart`, on appelle cela une _clé composite_.

### PHP
1. Créer l'entity `UserEntity` avec pour propriété publique les champs de la table `user`
2. Créer le repository `UserRepository`
3. Ajouter au repository la méthode publique `add()` prend un `User` en paramètre et l'insère dans la base de données
4. Ajouter au repository la méthode publique `get()` qui prend en paramètre l'`id` de l'`user` que l'on souhaite récupérer et qui retourne une instance de la classe `User`
5. Ajouter au repository la méthode publique `getAll()` qui sélectionne toute la table user et qui retourne un tableau de `User`
6. Faire de même pour les produits
